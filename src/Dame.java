import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Dame extends Piece{
    
    private String name;
    
    public Dame(int x, int y,boolean estBlanc, String name){
        super(x,y,estBlanc, name);
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }


    public boolean coupPossible(int x, int y){
        /**
        Args : int x : abcisse de la position choisi, 
                int y : ordonnée de la position choisi.

        Returns : boolean : retourne si la position choisi est possible.
         */
        
        // à completer
        return true;
    }

    @Override
    public Set<Case> coupPossible(Plateau plateau){
        Set<Case> listeCaseAttaque = new HashSet<>();

        List<Coordonnee> listeCoo = new ArrayList<>();
        listeCoo.add(new Coordonnee(1, 0));
        listeCoo.add(new Coordonnee(-1, 0));
        listeCoo.add(new Coordonnee(0, 1));
        listeCoo.add(new Coordonnee(0, -1));
        listeCoo.add(new Coordonnee(-1, -1));
        listeCoo.add(new Coordonnee(-1, 1));
        listeCoo.add(new Coordonnee(1, -1));
        listeCoo.add(new Coordonnee(1, 1));

        int colonne = this.getColonne();
        int ligne = this.getLigne();

        for(int indice = 0; indice<2; indice++){
            int valeurL = listeCoo.get(indice).getLigne();
            for(int l = ligne+valeurL; l < 8 && l >=0; l+=valeurL){
                if(plateau.caseVide(l, colonne) || plateau.getCase(l, colonne).getPiece().estBlanc() != this.estBlanc()){
                    listeCaseAttaque.add(new Case(true, l, colonne));
                    if(! plateau.caseVide(l, colonne) && plateau.getCase(l, colonne).getPiece().estBlanc() != this.estBlanc()){
                        break;                                  
                    }
                }
                else{break;} // La case que l'on regarde possède une piece allié et bloque le passage
            }
        }

        for(int indice2 = 2; indice2<4; indice2++){
            int valeurC = listeCoo.get(indice2).getColonne();
            for(int c = colonne+valeurC; c < 8 && c >=0; c+=valeurC){
                if(plateau.caseVide(ligne, c) || plateau.getCase(ligne, c).getPiece().estBlanc() != this.estBlanc()){
                    listeCaseAttaque.add(new Case(true, ligne, c));
                    if(! plateau.caseVide(ligne, c) && plateau.getCase(ligne, c).getPiece().estBlanc() != this.estBlanc()){
                        break;                                  
                    }
                }
                else{break;} // La case que l'on regarde possède une piece allié et bloque le passage
            } 
        }        

        colonne = this.getColonne();
        ligne = this.getLigne();

        for(int indice = 4; indice<8; indice++){
            int valeurL = listeCoo.get(indice).getLigne();
            int valeurC = listeCoo.get(indice).getColonne();
            int c = colonne+valeurC;
            for(int l = ligne+valeurL; l < 8 && l >=0 && c < 8 && c >=0; l+=valeurL){
                if(plateau.caseVide(l, c) || plateau.getCase(l, c).getPiece().estBlanc() != this.estBlanc()){
                    listeCaseAttaque.add(new Case(true, l, c));
                    if(! plateau.caseVide(l,c) && plateau.getCase(l, c).getPiece().estBlanc() != this.estBlanc()){
                        break;                                  
                    }
                    c+=valeurC;
                }
                else{break;} // La case que l'on regarde possède une piece allié et bloque le passage
            }
        }
        return listeCaseAttaque;
    }


    @Override
    public String toString(){
        if(this.estBlanc()){
            return "D";
        }
        else{
            return "d";
        }
    }
}
