import java.util.Optional;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.ImageView;

public class ControleurPromotion implements EventHandler<ActionEvent>{
    

    private String piece;
    private ChessGraphic vueEchec;
    private Coordonnee coo;


    public ControleurPromotion(ChessGraphic vueEchec, String piece, Coordonnee coo){
        this.piece = piece;
        this.vueEchec = vueEchec;
        this.coo = coo;
    }


    @Override
    public void handle(ActionEvent event){
        
        int ligne = coo.getLigne();
        int colonne = coo.getColonne();

        boolean estBlanc = this.vueEchec.getPlateau().getCase(ligne, colonne).getPiece().estBlanc();
        if(piece.equals("C")){
            this.vueEchec.getPlateau().getCase(ligne, colonne).setPiece(new Cavalier(ligne, colonne, estBlanc, "Cavalier"));
            if(estBlanc){
                vueEchec.getBoutons().get(coo.getLigne()).get(coo.getColonne()).setGraphic(Bouton.chargeUneImage(Bouton.chargeImage().get("C")));
            }
            else{
                vueEchec.getBoutons().get(coo.getLigne()).get(coo.getColonne()).setGraphic(Bouton.chargeUneImage(Bouton.chargeImage().get("c")));
            }
        }
        if(piece.equals("T")){
            this.vueEchec.getPlateau().getCase(ligne, colonne).setPiece(new Tour(ligne, colonne, estBlanc, "Tour"));
            if(estBlanc){
                vueEchec.getBoutons().get(coo.getLigne()).get(coo.getColonne()).setGraphic(Bouton.chargeUneImage(Bouton.chargeImage().get("T")));
            }
            else{
                vueEchec.getBoutons().get(coo.getLigne()).get(coo.getColonne()).setGraphic(Bouton.chargeUneImage(Bouton.chargeImage().get("t")));
            }
        }
        if(piece.equals("D")){
            this.vueEchec.getPlateau().getCase(ligne, colonne).setPiece(new Dame(ligne, colonne, estBlanc, "Dame"));
            if(estBlanc){
                vueEchec.getBoutons().get(coo.getLigne()).get(coo.getColonne()).setGraphic(Bouton.chargeUneImage(Bouton.chargeImage().get("D")));
            }
            else{
                vueEchec.getBoutons().get(coo.getLigne()).get(coo.getColonne()).setGraphic(Bouton.chargeUneImage(Bouton.chargeImage().get("d")));
            }
        }
        else if(piece.equals("F")){
            this.vueEchec.getPlateau().getCase(ligne, colonne).setPiece(new Fou(ligne, colonne, estBlanc, "Fou"));
            if(estBlanc){
                vueEchec.getBoutons().get(coo.getLigne()).get(coo.getColonne()).setGraphic(Bouton.chargeUneImage(Bouton.chargeImage().get("F")));
            }
            else{
                vueEchec.getBoutons().get(coo.getLigne()).get(coo.getColonne()).setGraphic(Bouton.chargeUneImage(Bouton.chargeImage().get("f")));
            }
        }
        // change la fenetre central de la scene par l'echequier
        this.vueEchec.getJeu().setCenter(this.vueEchec.getEchequier());

        // vérifie que la promotion n'entraine pas d'echec et mat
        if(this.vueEchec.getPartie().enEchec(this.vueEchec.getPlateau())  && this.vueEchec.getPartie().echecEtMat(this.vueEchec.getPlateau())){
            Optional<ButtonType> reponse = this.vueEchec.alertEchecEtMat(this.vueEchec.getPlateau().getCase(coo.getLigne(), coo.getColonne()).getPiece().estBlanc()).showAndWait();
            
            // si la réponse est oui
            if (reponse.isPresent() && reponse.get().equals(ButtonType.YES)){
                this.vueEchec.init();
            }
        }
        this.vueEchec.getPartie().setTraitAuBlanc();
        this.vueEchec.getPartie().setNbTours();
    }
}
