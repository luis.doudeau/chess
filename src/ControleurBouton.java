import java.util.Optional;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.CloseAction;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.ImageView;

public class ControleurBouton implements EventHandler<ActionEvent>{
    

    private Bouton bouton;
    private ChessGraphic vueEchec;


    public ControleurBouton(Bouton bouton, ChessGraphic vueEchec){
        this.bouton = bouton;
        this.vueEchec = vueEchec;
    }


    @Override
    public void handle(ActionEvent event){
        if( ! this.bouton.getCase().caseVide() && this.bouton.getCase().getPiece().estBlanc() == this.vueEchec.getPartie().traitAuBlanc()){
            if(! this.vueEchec.getCaseSelectionne().equals("")){
                this.vueEchec.getBoutons().get(Character.getNumericValue(this.vueEchec.getCaseSelectionne().charAt(0))).get(Character.getNumericValue(this.vueEchec.getCaseSelectionne().charAt(1))).setStyle("-fx-border-color: transparent;");
            }
        this.vueEchec.setCaseSelectionne(""+this.bouton.getCase().getLigne()+this.bouton.getCase().getColonne());
        this.bouton.setStyle("-fx-border-color: blue;");
        }

        else if(! this.vueEchec.getCaseSelectionne().equals("")){
            try{
                String res = ""+this.bouton.getCase().getLigne()+this.bouton.getCase().getColonne();

                //Appel du modèle
                String [] coupSplit = this.vueEchec.getPartie().lancerPartieGraphique(this.vueEchec.getPlateau(), this.vueEchec.getCaseSelectionne(), res);

                //MAJ de la vue (images)
                majVue(coupSplit);
            }   
            catch(ExceptionCoupPasPossible e){}
            catch(ExceptionEnEchec e2){}
            catch(ExceptionMauvaisTrait e3){}
            catch(ExceptionPromotion e4){
                this.vueEchec.getBoutons().get(Character.getNumericValue(this.vueEchec.getCaseSelectionne().charAt(0))).get(Character.getNumericValue(this.vueEchec.getCaseSelectionne().charAt(1))).setGraphic(null);
                this.vueEchec.fenetrePromotion(e4.getCoo());          
            }
            catch(ExceptionEchecEtMat e5){
                majVue(e5.getCoupSplit());
                Optional<ButtonType> reponse = this.vueEchec.alertEchecEtMat(e5.getVictoireBlanche()).showAndWait();
                // si la réponse est oui
                if (reponse.isPresent() && reponse.get().equals(ButtonType.YES)){
                    this.vueEchec.rejouer();
                    return;
                }
                this.vueEchec.getPartie().setTraitAuBlanc();
                this.vueEchec.getPartie().setNbTours();                
            }
            catch(ExceptionPriseEnPassant e6){
                this.vueEchec.getBoutons().get(e6.getCoo().getLigne()).get(e6.getCoo().getColonne()).setGraphic(null);
                this.vueEchec.getPlateau().deplacePiece(new Coordonnee(Integer.parseInt(e6.getCoupSplit()[0]), Integer.parseInt(e6.getCoupSplit()[1])), new Coordonnee(Integer.parseInt(e6.getCoupSplit()[2]), Integer.parseInt(e6.getCoupSplit()[3])));
                majVue(e6.getCoupSplit());
                this.vueEchec.getPartie().setDernierCoupJouer(new Coordonnee(Integer.parseInt(e6.getCoupSplit()[0]), Integer.parseInt(e6.getCoupSplit()[1])), new Coordonnee(Integer.parseInt(e6.getCoupSplit()[2]), Integer.parseInt(e6.getCoupSplit()[3])));
                this.vueEchec.getPartie().setTraitAuBlanc();
                this.vueEchec.getPartie().setNbTours();
            }

            this.vueEchec.getBoutons().get(Character.getNumericValue(this.vueEchec.getCaseSelectionne().charAt(0))).get(Character.getNumericValue(this.vueEchec.getCaseSelectionne().charAt(1))).setStyle("-fx-border-color: transparent;");
            this.vueEchec.setCaseSelectionne("");
        }
    }

    public void majVue(String [] coupSplit){
        this.vueEchec.getBoutons().get(Integer.parseInt(coupSplit[0])).get(Integer.parseInt(coupSplit[1])).setGraphic(null);
        ImageView imagePieceQuiBouge = Bouton.chargeUneImage(Bouton.chargeImage().get(this.vueEchec.getPlateau().getCase(Integer.parseInt(coupSplit[2]), Integer.parseInt(coupSplit[3])).getPiece().toString()));
        this.vueEchec.getBoutons().get(Integer.parseInt(coupSplit[2])).get(Integer.parseInt(coupSplit[3])).setGraphic(imagePieceQuiBouge);
    }
}
