
public class ExceptionPromotion extends Exception{

    private Coordonnee coo;
    
    public ExceptionPromotion(Coordonnee coo){
        this.coo = coo;
    }

    public Coordonnee getCoo() {
        return coo;
    }
}
