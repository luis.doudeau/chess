import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Pion extends Piece{


    private String name;

    public Pion(int x, int y, boolean estBlanc, String name){
        super(x,y,estBlanc,name);
        this.name = name;
    }

    @Override
    public int getColonne() {
        return super.getColonne();
    }

    @Override
    public int getLigne() {
        return super.getLigne();
    }

    public String getName() {
        return name;
    }

    @Override
    public Set<Case> coupPossible(Plateau plateau){
        if(this.estBlanc()){
            return coupPossibleBlanc(plateau);
        }
        else{
            return coupPossibleNoir(plateau);
        }
    }

    public Set<Case> priseEnPassantPourLesBlanc(Plateau plateau){
        Set<Case> listeCase = new HashSet<>();
        if(Jeu.dernierCoupJouer.size() != 0 && plateau.getPlateau().getCase(Jeu.dernierCoupJouer.get(1).getLigne(), Jeu.dernierCoupJouer.get(1).getColonne()).getPiece().getClass().equals(Pion.class) && Jeu.dernierCoupJouer.get(1).getLigne() == 4 && Jeu.dernierCoupJouer.get(0).getLigne() == 6){
            if( this.getLigne() == 4 && this.getColonne() == Jeu.dernierCoupJouer.get(1).getColonne()-1 && this.estBlanc()){
                listeCase.add(new Case(true, 5, this.getColonne()+1));
            }
            if( this.getLigne() == 4 && this.getColonne() == Jeu.dernierCoupJouer.get(1).getColonne()+1 && this.estBlanc()){
                listeCase.add(new Case(true, 5, this.getColonne()-1));
            }
        }
        return listeCase;
    }

    public Set<Case> priseEnPassantPourLesNoir(Plateau plateau){
        Set<Case> listeCase = new HashSet<>();
        if(Jeu.dernierCoupJouer.size() != 0 && plateau.getPlateau().getCase(Jeu.dernierCoupJouer.get(1).getLigne(), Jeu.dernierCoupJouer.get(1).getColonne()).getPiece().getClass().equals(Pion.class) && Jeu.dernierCoupJouer.get(1).getLigne() == 3 && Jeu.dernierCoupJouer.get(0).getLigne() == 1){
            if( this.getLigne() == 3 && this.getColonne() == Jeu.dernierCoupJouer.get(1).getColonne()-1 && ! this.estBlanc()){
                listeCase.add(new Case(true, 2, this.getColonne()+1));
            }
            if( this.getLigne() == 3 && this.getColonne() == Jeu.dernierCoupJouer.get(1).getColonne()+1 && ! this.estBlanc()){
                listeCase.add(new Case(true, 2, this.getColonne()-1));
            }
        }
        return listeCase;
    }

    
    public Set<Case> coupPossibleBlanc(Plateau plateau){
        Set<Case> listeCaseAttaque = new HashSet<>();
        if(Plateau.estSurLePlateau(this.getLigne()+2, this.getColonne()) && (this.estBlanc() && this.getLigne() == 1 && plateau.caseVide(this.getLigne()+2, this.getColonne()) && plateau.getPlateau().caseVide(this.getLigne()+1, this.getColonne()))){
            listeCaseAttaque.add(new Case(true, this.getLigne()+2, this.getColonne()));
        }
        if(Plateau.estSurLePlateau(this.getLigne()+1, this.getColonne()) && plateau.caseVide(this.getLigne()+1, this.getColonne())){
            listeCaseAttaque.add(new Case(true, this.getLigne()+1, this.getColonne()));
        }
        if(Plateau.estSurLePlateau(this.getLigne()+1, this.getColonne()-1) && !plateau.caseVide(this.getLigne()+1, this.getColonne()-1) && plateau.getCase(this.getLigne()+1, this.getColonne()-1).getPiece().estBlanc() != this.estBlanc()){
            listeCaseAttaque.add(new Case(true, this.getLigne()+1, this.getColonne()-1));
        }
        if(Plateau.estSurLePlateau(this.getLigne()+1, this.getColonne()+1) && !plateau.caseVide(this.getLigne()+1, this.getColonne()+1) && plateau.getCase(this.getLigne()+1, this.getColonne()+1).getPiece().estBlanc() != this.estBlanc()){
            listeCaseAttaque.add(new Case(true, this.getLigne()+1, this.getColonne()+1));
        }
        listeCaseAttaque.addAll(priseEnPassantPourLesBlanc(plateau));

        return listeCaseAttaque;
    }


    
    public Set<Case> coupPossibleNoir(Plateau plateau){
        Set<Case> listeCaseAttaque = new HashSet<>();
        if(Plateau.estSurLePlateau(this.getLigne()-2, this.getColonne()) && (!this.estBlanc() && this.getLigne() == 6 && plateau.caseVide(this.getLigne()-2, this.getColonne()) && plateau.caseVide(this.getLigne()-1, this.getColonne()))){
            listeCaseAttaque.add(new Case(true, this.getLigne()-2, this.getColonne()));
        }
        if(Plateau.estSurLePlateau(this.getLigne()-1, this.getColonne()) && plateau.caseVide(this.getLigne()-1, this.getColonne())){
            listeCaseAttaque.add(new Case(true, this.getLigne()-1, this.getColonne()));
        }
        if(Plateau.estSurLePlateau(this.getLigne()-1, this.getColonne()-1) && !plateau.caseVide(this.getLigne()-1, this.getColonne()-1) && plateau.getCase(this.getLigne()-1, this.getColonne()-1).getPiece().estBlanc() != this.estBlanc()){
            listeCaseAttaque.add(new Case(true, this.getLigne()-1, this.getColonne()-1));
        }
        if(Plateau.estSurLePlateau(this.getLigne()-1, this.getColonne()+1) && !plateau.caseVide(this.getLigne()-1, this.getColonne()+1) && plateau.getCase(this.getLigne()-1, this.getColonne()+1).getPiece().estBlanc() != this.estBlanc()){
            listeCaseAttaque.add(new Case(true, this.getLigne()-1, this.getColonne()+1));
        }
        listeCaseAttaque.addAll(priseEnPassantPourLesNoir(plateau));

        return listeCaseAttaque;
    }
    

    @Override
    public String toString(){
        if(this.estBlanc()){
            return "P";
        }
        else{
            return "p";
        }
    }

}
