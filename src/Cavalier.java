import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Cavalier extends Piece {
    
    private String name;
    
    public Cavalier(int x, int y,boolean estBlanc, String name){
        super(x,y,estBlanc,name);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public Set<Case> coupPossible(Plateau plateau){
        /**
        Args : int x : abcisse de la position choisi, 
                int y : ordonnée de la position choisi.

        Returns : boolean : retourne si la position choisi est possible.
         */
        
        // à completer
        Set<Case> listeCaseAttaque = new HashSet<>();
        List<Coordonnee> listeCoord = new ArrayList<>();

        // Coordonné possible du deplécement du Cavalier
        listeCoord.add(new Coordonnee(-1, -2));
        listeCoord.add(new Coordonnee(-1, 2));
        listeCoord.add(new Coordonnee(1, -2));
        listeCoord.add(new Coordonnee(1, 2));
        listeCoord.add(new Coordonnee(-2, -1));
        listeCoord.add(new Coordonnee(-2, 1));
        listeCoord.add(new Coordonnee(2, -1));
        listeCoord.add(new Coordonnee(2, 1));

        for(Coordonnee coo : listeCoord){
            int ligne = coo.getLigne() + this.getLigne();
            int colonne = coo.getColonne() + this.getColonne();
            if(Plateau.estSurLePlateau(ligne, colonne)){
                if(plateau.caseVide(ligne,colonne) || plateau.getCase(ligne, colonne).getPiece().estBlanc() != this.estBlanc()){
                    listeCaseAttaque.add(new Case(true, ligne, colonne));
                }
            }
        } 

        return listeCaseAttaque;
    }

    @Override
    public String toString(){
        if(this.estBlanc()){
            return "C";
        }
        else{
            return "c";
        }
    }
}