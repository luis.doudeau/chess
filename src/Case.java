public class Case {
    
    private boolean estBlanc;
    private InterfacePiece piece;
    private int ligne;
    private int colonne;

    public Case(boolean couleur, int ligne, int colonne){
        this.estBlanc = couleur;
        this.ligne = ligne;
        this.colonne =  colonne;
        this.piece = null;
    }

    public boolean estBlanc(){
        return this.estBlanc;
    }

    public InterfacePiece getPiece(){
        return this.piece;
    }

    public int getLigne() {
        return ligne;
    }

    public int getColonne() {
        return colonne;
    }

    public void setPiece(InterfacePiece newPiece){
        this.piece = newPiece;
    }

    public void enlevePiece(){
        this.piece = null;
    }

    public boolean caseVide(){
        return this.piece == null;
    }

    @Override
    public boolean equals(Object o){
        if(o == this){
            return true;
        }
        if(! (o instanceof Case)){
            return false;
        }
        Case c = (Case) o;
        return c.getColonne() == this.getColonne() && c.getLigne() == this.getLigne();
    }

    public String affichagePiece(){
        if(this.getPiece() == null){
            return "";
        }
        else{
            return this.getPiece().toString();
        }
    }

    @Override
    public int hashCode(){
        return this.colonne+7*this.ligne;
    }

    @Override
    public String toString(){
        return "("+ligne+","+colonne+")";
    }
}
