import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Tour extends Piece{

    private String name;
    private boolean pasBouge = true;

    public Tour(int x, int y,boolean estBlanc, String name){
        super(x,y,estBlanc,name);
        this.name = name;
    }

    public void setPasBouge(boolean pasBouge) {
        this.pasBouge = !pasBouge;
    }

    public boolean getPasBouge(){
        return this.pasBouge;
    }

    public String getName() {
        return name;
    }


    @Override
    public Set<Case> coupPossible(Plateau plateau){
        Set<Case> listeCaseAttaque = new HashSet<>();

        List<Coordonnee> listeCoo = new ArrayList<>();
        listeCoo.add(new Coordonnee(1, 0));
        listeCoo.add(new Coordonnee(-1, 0));
        listeCoo.add(new Coordonnee(0, 1));
        listeCoo.add(new Coordonnee(0, -1));

        int colonne = this.getColonne();
        int ligne = this.getLigne();

        for(int indice = 0; indice<2; indice++){
            int valeurL = listeCoo.get(indice).getLigne();
            for(int l = ligne+valeurL; l < 8 && l >=0; l+=valeurL){
                if(plateau.caseVide(l, colonne) || plateau.getCase(l, colonne).getPiece().estBlanc() != this.estBlanc()){
                    listeCaseAttaque.add(new Case(true, l, colonne));
                    if(! plateau.caseVide(l, colonne) && plateau.getCase(l, colonne).getPiece().estBlanc() != this.estBlanc()){
                        break;                                  
                    }
                }
                else{break;} // La case que l'on regarde possède une piece allié et bloque le passage
            }
        }

        for(int indice2 = 2; indice2<4; indice2++){
            int valeurC = listeCoo.get(indice2).getColonne();
            for(int c = colonne+valeurC; c < 8 && c >=0; c+=valeurC){
                if(plateau.caseVide(ligne, c) || plateau.getCase(ligne, c).getPiece().estBlanc() != this.estBlanc()){
                    listeCaseAttaque.add(new Case(true, ligne, c));
                    if(! plateau.caseVide(ligne, c) && plateau.getCase(ligne, c).getPiece().estBlanc() != this.estBlanc()){
                        break;                                  
                    }
                }
                else{break;} // La case que l'on regarde possède une piece allié et bloque le passage
            } 
        }

        return listeCaseAttaque;
    }


    @Override
    public String toString(){
        if(this.estBlanc()){
            return "T";
        }
        else{
            return "t";
        }
    }
}



// for(Coordonnee coo : listeCoo){
//     ligne = this.getLigne();
//     colonne = this.getColonne();
//     for(int iteration1 = this.getLigne()+coo.getLigne(); iteration1<8 || iteration1 >=0 || ligne<8 || ligne >=0 ; iteration1++){
//         ligne+=coo.getLigne();
//         for(int iteration2 = this.getColonne()+coo.getColonne(); iteration2<8 || iteration2 >=0 ||  colonne<8 || colonne >=0; iteration2++){
//             colonne +=coo.getColonne();
//             if(this.caseVide(this.getLigne(), colonne) || this.getCase(this.getLigne(), colonne).getPiece().estBlanc() != this.estBlanc()){
//                 listeCaseAttaque.add(new Case(true, this.getLigne(), colonne));
//                 if(this.getCase(this.getLigne(), colonne).getPiece().estBlanc() != this.estBlanc()){
//                     break;                                  
//                 }
//             }
//             else{break;} // La case que l'on regarde possède une piece allié et bloque le passage
//         }
//     }