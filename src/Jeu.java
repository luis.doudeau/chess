import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class Jeu {
    
    private int nbTours;
    private boolean traitAuBlanc;
    public static List<Coordonnee> dernierCoupJouer;

    public Jeu(){
        this.nbTours = 0;
        this.traitAuBlanc = true;
        Jeu.dernierCoupJouer = new ArrayList<>();
    }

    public int getNbTours() {
        return nbTours;
    }

    public List<Coordonnee> getDernierCoupJouer() {
        return dernierCoupJouer;
    }

    public boolean traitAuBlanc(){
        return this.traitAuBlanc;
    }
    

    public String [] demandeCoup(Plateau plateau){
        System.out.print("Choisi la case de la pièce à déplacer, puis sa nouvelle case\nPar exemple : D2D4 → ");
        Scanner demandeCoup = new Scanner(System.in);
        String [] coupSplit = demandeCoup.next().split("");
        while( ! verifCoup(coupSplit) || !coupLegal(plateau, coupSplit) || ! verifTrait(plateau, coupSplit) || echecAuRoi(plateau, coupSplit)){
            System.out.print("Choisi la case de la pièce à déplacer, puis sa nouvelle case\nPar exemple : D2D4 → ");
            demandeCoup = new Scanner(System.in);
            coupSplit = demandeCoup.next().split("");
        }
        return coupSplit;
    }

    public boolean verifCoup(String [] coup){
        List<String> listeLettre = new ArrayList<>(Arrays.asList("A","B","C","D","E","F","G","H"));
        try{
            if(coup.length == 4 && listeLettre.contains(coup[0]) && Integer.parseInt(coup[1]) > 0 && Integer.parseInt(coup[1]) <= 8 && listeLettre.contains(coup[2]) && Integer.parseInt(coup[3]) > 0 && Integer.parseInt(coup[3]) <= 8){
                return true;
            }
            return false;
        }
        catch(NumberFormatException e){
            System.out.println("\nLe format du coup est incorrect ! ");
            return false;
        }        
    }


    private boolean coupLegal(Plateau plateau, String[] coupSplit) {
        if (plateau.caseVide(coupEnCoordonnees(coupSplit[1]), coupEnCoordonnees(coupSplit[0]))){
            System.out.println("La case choisie est vide !");
            return false;
        }
        return true;
    }

    private boolean verifTrait(Plateau plateau, String[] coupSplit) {
        if(! plateau.getCase(coupEnCoordonnees(coupSplit[1]), coupEnCoordonnees(coupSplit[0])).getPiece().estBlanc() == traitAuBlanc){
            if(traitAuBlanc){
                System.out.println("Trait au Blanc !");
            }
            else{
                System.out.println("Trait au Noir !");
            }
            return false;
        }
        return true;
    }


    public Coordonnee verifPromotion(Plateau plateau){
        Coordonnee cooPionPromu = new Coordonnee(-1, -1);
        List<Integer> listeIndice = new ArrayList<>(Arrays.asList(0,7)); 
        for(Integer ligne : listeIndice){
            for(int colonne=0; colonne<8; colonne++){
                if(! plateau.caseVide(ligne, colonne) && plateau.getCase(ligne, colonne).getPiece() instanceof Pion){
                    cooPionPromu.setColonne(colonne);
                    cooPionPromu.setLigne(ligne);
                    return cooPionPromu;
                }
            }
        }
        return cooPionPromu;
    }


    private boolean echecAuRoi(Plateau plateau, String [] coupSplit) {
        InterfacePiece pieceRemplacer = plateau.getCase(coupEnCoordonnees(coupSplit[3]), coupEnCoordonnees(coupSplit[2])).getPiece();
        plateau.deplacePiece(new Coordonnee(coupEnCoordonnees(coupSplit[1]), coupEnCoordonnees(coupSplit[0])), new Coordonnee(coupEnCoordonnees(coupSplit[3]), coupEnCoordonnees(coupSplit[2])));
        for(int ligne=0; ligne<8; ligne++){
            for(int colonne = 0; colonne<8; colonne++){
                if(! plateau.caseVide(ligne, colonne) && plateau.getCase(ligne, colonne).getPiece().getName().equals("Roi") && plateau.getCase(ligne, colonne).getPiece().estBlanc() == traitAuBlanc){
                    Roi roi = (Roi) plateau.getCase(ligne, colonne).getPiece();
                    if(roi.enEchecRoi(plateau)){
                        System.out.println("Le roi est en echec !");
                        plateau.deplacePiece(new Coordonnee(coupEnCoordonnees(coupSplit[3]), coupEnCoordonnees(coupSplit[2])), new Coordonnee(coupEnCoordonnees(coupSplit[1]), coupEnCoordonnees(coupSplit[0])));
                        plateau.getCase(coupEnCoordonnees(coupSplit[3]), coupEnCoordonnees(coupSplit[2])).setPiece(pieceRemplacer);
                        return true;
                    }
                }
            }
        }
        plateau.deplacePiece(new Coordonnee(coupEnCoordonnees(coupSplit[3]), coupEnCoordonnees(coupSplit[2])), new Coordonnee(coupEnCoordonnees(coupSplit[1]), coupEnCoordonnees(coupSplit[0])));
        plateau.getCase(coupEnCoordonnees(coupSplit[3]), coupEnCoordonnees(coupSplit[2])).setPiece(pieceRemplacer);
        return false;
    }


    public void lancerPartieTerminal(){
        Plateau plateau = new Plateau();
        int lignePiece;
        int ligneCase;
        int colonnePiece;
        int colonneCase;
        String [] res;
        while(true){
            System.out.println(plateau);
            res = demandeCoup(plateau); 
            lignePiece = coupEnCoordonnees(res[1]);
            ligneCase = coupEnCoordonnees(res[3]);
            colonnePiece = coupEnCoordonnees(res[0]);
            colonneCase = coupEnCoordonnees(res[2]);
            System.out.println(plateau.getCase(lignePiece, colonnePiece).getPiece().coupPossible(plateau));
            while(!plateau.getCase(lignePiece, colonnePiece).getPiece().coupPossible(plateau).contains(new Case(true, ligneCase, colonneCase))){
                System.out.println("Coup impossible ! ");
                res = demandeCoup(plateau);
                lignePiece = Integer.parseInt(res[1])-1;
                ligneCase = Integer.parseInt(res[3])-1;
                colonnePiece = Plateau.lettrePlateau().indexOf(res[0]);
                colonneCase = Plateau.lettrePlateau().indexOf(res[2]);
            }
            plateau.deplacePiece(new Coordonnee(lignePiece, colonnePiece), new Coordonnee(ligneCase, colonneCase));
            setNbTours();
            setTraitAuBlanc();
            }   
    }

    public String [] lancerPartieGraphique(Plateau plateau, String ancienneCoo, String nouvelleCoo) throws ExceptionCoupPasPossible, ExceptionEnEchec, ExceptionMauvaisTrait, ExceptionPromotion, ExceptionEchecEtMat, ExceptionPriseEnPassant{
        int lignePiece;
        int ligneCase;
        int colonnePiece;
        int colonneCase;

        lignePiece = Character.getNumericValue(ancienneCoo.charAt(0));
        ligneCase = Character.getNumericValue(nouvelleCoo.charAt(0));
        colonnePiece = Character.getNumericValue(ancienneCoo.charAt(1));
        colonneCase = Character.getNumericValue(nouvelleCoo.charAt(1));
                
        if(! plateau.getCase(lignePiece, colonnePiece).getPiece().coupPossible(plateau).contains(new Case(true, ligneCase, colonneCase))){
            throw new ExceptionCoupPasPossible();
        }

        String [] coupSplit = (""+ Plateau.lettrePlateau().get(colonnePiece).toString() + ""+(lignePiece+1) + ""+Plateau.lettrePlateau().get(colonneCase).toString() + ""+(ligneCase+1)).split("");
        String [] coupSplit2 = ("" + lignePiece + colonnePiece+ ligneCase + colonneCase).split("");
        if(! verifTrait(plateau, coupSplit)){
            throw new ExceptionMauvaisTrait();
        }
        if(echecAuRoi(plateau, coupSplit)){
            throw new ExceptionEnEchec();
        }
        Coordonnee ancienneCoordonnee = new Coordonnee(lignePiece, colonnePiece);
        Coordonnee nouvelleCoordonnee = new Coordonnee(ligneCase, colonneCase);

        if(verifPriseEnPassant(plateau, ancienneCoordonnee, nouvelleCoordonnee)){
            throw new ExceptionPriseEnPassant(priseEnPassant(plateau, ancienneCoordonnee, nouvelleCoordonnee), coupSplit2);
        }
        setDernierCoupJouer(ancienneCoordonnee, nouvelleCoordonnee);


        plateau.deplacePiece(ancienneCoordonnee, nouvelleCoordonnee);

        Coordonnee coo = verifPromotion(plateau);
        if( coo.getLigne() !=-1){
            throw new ExceptionPromotion(coo);
        }
        if(enEchec(plateau) && echecEtMat(plateau)){
            throw new ExceptionEchecEtMat(traitAuBlanc, coupSplit2);
        }
        setNbTours();
        setTraitAuBlanc();
        return coupSplit2;
    }


    public Coordonnee priseEnPassant(Plateau plateau, Coordonnee ancienneCoo, Coordonnee nouvelleCoo){
        Coordonnee coo = new Coordonnee(-1, -1);
    
        if(plateau.getCase(ancienneCoo.getLigne(), ancienneCoo.getColonne()).getPiece().estBlanc()){
            plateau.getCase(nouvelleCoo.getLigne()-1, nouvelleCoo.getColonne()).enlevePiece();
            coo.setLigne(nouvelleCoo.getLigne()-1);
            coo.setColonne(nouvelleCoo.getColonne());
            return coo;
        }
        else{
            plateau.getCase(nouvelleCoo.getLigne()+1, nouvelleCoo.getColonne()).enlevePiece();
            coo.setLigne(nouvelleCoo.getLigne()+1);
            coo.setColonne(nouvelleCoo.getColonne());
            return coo;
        }
    }

    public boolean verifPriseEnPassant(Plateau plateau, Coordonnee ancienneCoo, Coordonnee nouvelleCoo){
        return plateau.getCase(ancienneCoo.getLigne(), ancienneCoo.getColonne()).getPiece().getClass().equals(Pion.class) && ancienneCoo.getColonne() != nouvelleCoo.getColonne() && plateau.caseVide(nouvelleCoo.getLigne(), nouvelleCoo.getColonne());
    }


    public void setDernierCoupJouer(Coordonnee lastCoo, Coordonnee newCoo){
        Jeu.dernierCoupJouer.clear();
        Jeu.dernierCoupJouer.add(lastCoo);
        Jeu.dernierCoupJouer.add(newCoo);
    }


    public boolean enEchec(Plateau plateau) {
        for(int ligne=0; ligne<8; ligne++){
            for(int colonne = 0; colonne<8; colonne++){
                if(! plateau.caseVide(ligne, colonne) && plateau.getCase(ligne, colonne).getPiece().getName().equals("Roi") && plateau.getCase(ligne, colonne).getPiece().estBlanc() != traitAuBlanc){
                    Roi roi = (Roi) plateau.getCase(ligne, colonne).getPiece();
                    if(roi.enEchecRoi(plateau)){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean echecEtMat(Plateau plateau) {
        for(int ligne=0; ligne<8; ligne++){
            for(int colonne = 0; colonne<8; colonne++){
                if(! plateau.caseVide(ligne, colonne) && plateau.getCase(ligne, colonne).getPiece().estBlanc() != traitAuBlanc){
                    for(Case coup : plateau.getCase(ligne, colonne).getPiece().coupPossible(plateau)){
                        InterfacePiece pieceRemplacer = null;
                        if(! plateau.caseVide(coup.getLigne(), coup.getColonne())){
                            pieceRemplacer = plateau.getCase(coup.getLigne(), coup.getColonne()).getPiece();
                        }
                        plateau.deplacePiece(new Coordonnee(ligne, colonne), new Coordonnee(coup.getLigne(), coup.getColonne()));
                        if(! enEchec(plateau)){
                            plateau.deplacePiece(new Coordonnee(coup.getLigne(), coup.getColonne()), new Coordonnee(ligne, colonne));
                            plateau.getCase(coup.getLigne(), coup.getColonne()).setPiece(pieceRemplacer);
                            return false;
                        }
                        plateau.deplacePiece(new Coordonnee(coup.getLigne(), coup.getColonne()), new Coordonnee(ligne, colonne));
                        plateau.getCase(coup.getLigne(), coup.getColonne()).setPiece(pieceRemplacer);
                            
                    }
                }
            }
        }
        System.out.println("Echec ... ET MAT !");
        return true;
    }


    public Integer coupEnCoordonnees(String coupSplit){
        try{
            return Integer.parseInt(coupSplit)-1;
        }
        catch(NumberFormatException e){
            return Plateau.lettrePlateau().indexOf(coupSplit);
        }
    }
    

    public void setTraitAuBlanc(){
        this.traitAuBlanc = !traitAuBlanc;
    }

    public void setNbTours(){
        this.nbTours ++;
    }

}
