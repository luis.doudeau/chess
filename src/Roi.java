import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Roi extends Piece {
    
    private String name;
    private boolean pasBouge = true;
    
    public Roi(int x, int y,boolean estBlanc, String name){
        super(x,y,estBlanc, name);
        this.name = name;
    }

    public void setPasBouge(boolean pasBouge) {
        this.pasBouge = !pasBouge;
    }

    public boolean getPasBouge(){
        return this.pasBouge;
    }

    public String getName() {
        return name;
    }


    public void setPremierCoup(){
        /**
         * Fonction qui modifie l'attribut pasBouge à faux si le roi vien de bouger pour la premère fois
        
        Args : No Args

        Returns : void : 
         */
        
    
        pasBouge = false;
    }

    public HashMap<String, Set<Case>> caseAttaque(Plateau plateau){
        
        Set<Case> ensembleCaseAttaqueParLesNoirs = new HashSet<>();
        Set<Case> ensembleCaseAttaqueParLesBlancs = new HashSet<>();
        HashMap<String, Set<Case>> dicoCaseAttaque = new HashMap<>();
        dicoCaseAttaque.put("Blancs", ensembleCaseAttaqueParLesBlancs);
        dicoCaseAttaque.put("Noirs", ensembleCaseAttaqueParLesNoirs);

        // Parcour du plateau
        for(int ligne = 0; ligne<8; ligne++){
            for(int colonne = 0; colonne<8; colonne++){
                if(! plateau.caseVide(ligne, colonne)){
                    if( !plateau.getCase(ligne, colonne).getPiece().estBlanc()){
                        ensembleCaseAttaqueParLesNoirs.addAll(plateau.getCase(ligne, colonne).getPiece().coupPossible(plateau));
                    }
                    else{
                        ensembleCaseAttaqueParLesBlancs.addAll(plateau.getCase(ligne, colonne).getPiece().coupPossible(plateau));
                    }
                }
            }
        }
        return dicoCaseAttaque;
    }

    public boolean enEchecRoi(Plateau plateau){
        /**
        Args : No Args

        Returns : boolean : retourne si le roi est actuellement en echec (true ou false)
         */
        
        if(this.estBlanc()){
            return caseAttaque(plateau).get("Noirs").contains(new Case(true, this.getLigne(), this.getColonne()));
        }
        else{
            return caseAttaque(plateau).get("Blancs").contains(new Case(true, this.getLigne(), this.getColonne()));
        }
    }
    

    public boolean estPat(Plateau plateau){
        /**
        
        Args : No Args

        Returns : boolean : retourne si le roi est actuellement pat (true ou false)

        Pour rappelle, le pat est le fait que le roi ne peut plus bouger mais qu'il ne soit pas en echec
         */
        
        // à completer
        return true;
    }

    
    @Override
    public Set<Case> coupPossible(Plateau plateau){
        Set<Case> listeCaseAttaque = new HashSet<>();
        List<Coordonnee> listeCoord = new ArrayList<>();

        // Coordonné possible du deplécement du Cavalier
        listeCoord.add(new Coordonnee(-1, -1));
        listeCoord.add(new Coordonnee(-1, 0));
        listeCoord.add(new Coordonnee(-1, 1));
        listeCoord.add(new Coordonnee(0, 1));
        listeCoord.add(new Coordonnee(1, 1));
        listeCoord.add(new Coordonnee(1, 0));
        listeCoord.add(new Coordonnee(1, -1));
        listeCoord.add(new Coordonnee(0, -1));

        for(Coordonnee coo : listeCoord){
            int ligne = coo.getLigne()+this.getLigne();
            int colonne = coo.getColonne()+this.getColonne();            
            if(Plateau.estSurLePlateau(ligne, colonne)){
                if(plateau.caseVide(ligne, colonne) || plateau.getCase(ligne, colonne).getPiece().estBlanc() != this.estBlanc())
                    listeCaseAttaque.add(new Case(true, ligne, colonne));
            }
        }

        return listeCaseAttaque;
    }

    @Override
    public String toString(){
        if(this.estBlanc()){
            return "R";
        }
        else{
            return "r";
        }
    }
}