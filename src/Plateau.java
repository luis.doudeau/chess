import java.util.ArrayList;
import java.util.List;


public class Plateau extends ArrayList<ArrayList<Case>>{
        

    public Plateau(){  // creer le plateau vide
        super();
        initPlateau();
    }

        
    public void initPlateau(){
        boolean estBlanc = true;
        for(int ligne = 0; ligne<8; ligne ++){
            ArrayList<Case> listeLigne = new ArrayList<>(); 
            for(int colonne = 0; colonne<8; colonne++){
                listeLigne.add(new Case(estBlanc,ligne, colonne));
                estBlanc = !estBlanc;
            }
            this.add(listeLigne);
            estBlanc = !estBlanc;
        }
        getCase(0, 0).setPiece(new Tour(0,0,true, "Tour"));
        getCase(0, 1).setPiece(new Cavalier(0,1,true,"Cavalier"));
        getCase(0, 2).setPiece(new Fou(0,2,true, "Fou"));
        getCase(0, 3).setPiece(new Roi(0,3,true,"Roi"));
        getCase(0, 4).setPiece(new Dame(0,4,true,"Dame"));
        getCase(0, 5).setPiece(new Fou(0,5,true,"Fou"));
        getCase(0, 6).setPiece(new Cavalier(0,6,true,"Cavalier"));
        getCase(0, 7).setPiece(new Tour(0,7,true, "Tour"));

        for(int i=0; i<8;++i){
            getCase(1, i).setPiece(new Pion(1,i,true, "Pion"));
        }
        for(int i=0; i<8;++i){
            getCase(6, i).setPiece(new Pion(6,i,false,"Pion"));
        }
        getCase(7, 0).setPiece(new Tour(7,0,false,"Tour"));
        getCase(7, 1).setPiece(new Cavalier(7,1,false,"Cavalier"));
        getCase(7, 2).setPiece(new Fou(7,2,false,"Fou"));
        getCase(7, 3).setPiece(new Roi(7,3,false,"Roi"));
        getCase(7, 4).setPiece(new Dame(7,4,false,"Dame"));
        getCase(7, 5).setPiece(new Fou(7,5,false,"Fou"));
        getCase(7, 6).setPiece(new Cavalier(7,6,false,"Cavalier"));
        getCase(7, 7).setPiece(new Tour(7,7,false,"Tour"));
    }


    public Case getCase(int ligne, int colonne){
        return this.get(ligne).get(colonne);
    }

    public boolean caseVide(int ligne, int colonne){
        return this.get(ligne).get(colonne).getPiece() == null;
    }


    public Plateau getPlateau(){
        return this;
    }


    public void deplacePiece(Coordonnee ancienneCoo, Coordonnee nouvelleCoo){
        this.getCase(ancienneCoo.getLigne(), ancienneCoo.getColonne()).getPiece().setLigne(nouvelleCoo.getLigne());
        this.getCase(ancienneCoo.getLigne(), ancienneCoo.getColonne()).getPiece().setColonne(nouvelleCoo.getColonne());
        InterfacePiece pieceQuiSeDeplace = this.getCase(ancienneCoo.getLigne(), ancienneCoo.getColonne()).getPiece();
        this.getCase(ancienneCoo.getLigne(), ancienneCoo.getColonne()).enlevePiece();
        this.getCase(nouvelleCoo.getLigne(), nouvelleCoo.getColonne()).setPiece(pieceQuiSeDeplace);
    }


    public static boolean estSurLePlateau(int ligne, int colonne){
        return ligne >= 0 && ligne < 8 && colonne >= 0 && colonne < 8;
    }

    public void setCase(int ligne, int colonne, Case newCase){
        this.get(ligne).set(colonne, newCase);
    }

    public static List<String> lettrePlateau(){
        List<String> listeLettre = new ArrayList<>();
        listeLettre.add("A");
        listeLettre.add("B");
        listeLettre.add("C");
        listeLettre.add("D");
        listeLettre.add("E");
        listeLettre.add("F");
        listeLettre.add("G");
        listeLettre.add("H");
        return listeLettre;
    }

    @Override
    public String toString(){
        String affichage = "\n    ";
            for(int colonne = 0; colonne<8; ++colonne){
                affichage += " "+ Plateau.lettrePlateau().get(colonne) + "  ";
            }
            affichage += "\n" + "   ┌";
            for(int colonne = 0; colonne<7; ++colonne){
                affichage += "───┬";
            }
            affichage += "───┐"+"\n";
            for(int ligne = 0; ligne<8;++ligne){
                affichage += "" + (ligne+1) + "  │";
                for(int colonne = 0; colonne<8; ++colonne){
                    if(getCase(ligne, colonne).getPiece() != null){
                        affichage +=" "+ getCase(ligne, colonne).getPiece().toString()+" │";
                    }
                    else{
                        affichage += "   │";
                    }
                }
                affichage += "\n";
                if(ligne==7){
                    affichage += "   └";
                    for(int i = 0; i<7; ++i){
                        affichage += "───┴";
                    }
                    affichage += "───┘";
                }else{
                    affichage +="   ├";
                    for(int i = 0; i<7; ++i){
                        affichage += "───┼";
                    }
                    affichage += "───┤\n";
                }
        }
        return affichage;
    }
}

