import java.util.Set;

public abstract class Piece implements InterfacePiece {
    
    private int ligne;
    private int colonne;
    private boolean estBlanc;
    private String name;

    public Piece(int ligne, int colonne,boolean estBlanc, String name){
        this.ligne = ligne;
        this.colonne = colonne;
        this.estBlanc  = estBlanc;
    }

    @Override
    public boolean estBlanc(){
        return this.estBlanc;
    }

    @Override
    public void setColonne(int colonne) {
        this.colonne = colonne;
    }

    @Override
    public void setLigne(int ligne) {
        this.ligne = ligne;
    }

    @Override
    public int getLigne(){
        return this.ligne;
    }

    @Override
    public int getColonne(){
        return this.colonne;
    }

    public abstract Set<Case> coupPossible(Plateau plateau);

    @Override
    public abstract String toString();

    public String getName() {
        return name;
    }
}
