import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Fou extends Piece{
    
    private String name;

    public Fou(int ligne, int colonne,boolean estBlanc, String name){
        super(ligne, colonne, estBlanc, name);
        this.name = name;
    }

    @Override
    public int getColonne() {
        return super.getColonne();
    }

    @Override
    public int getLigne() {
        return super.getLigne();
    }

    public String getName() {
        return name;
    }

    @Override
    public Set<Case> coupPossible(Plateau plateau){
        Set<Case> listeCaseAttaque = new HashSet<>();

        List<Coordonnee> listeCoo = new ArrayList<>();
        listeCoo.add(new Coordonnee(-1, -1));
        listeCoo.add(new Coordonnee(-1, 1));
        listeCoo.add(new Coordonnee(1, -1));
        listeCoo.add(new Coordonnee(1, 1));

        int colonne = this.getColonne();
        int ligne = this.getLigne();

        for(int indice = 0; indice<4; indice++){
            int valeurL = listeCoo.get(indice).getLigne();
            int valeurC = listeCoo.get(indice).getColonne();
            int c = colonne+valeurC;
            for(int l = ligne+valeurL; l < 8 && l >=0 && c < 8 && c >=0; l+=valeurL){
                if(plateau.caseVide(l, c) || plateau.getCase(l, c).getPiece().estBlanc() != this.estBlanc()){
                    listeCaseAttaque.add(new Case(true, l, c));
                    if(! plateau.caseVide(l,c) && plateau.getCase(l, c).getPiece().estBlanc() != this.estBlanc()){
                        break;                                  
                    }
                    c+=valeurC;
                }
                else{break;} // La case que l'on regarde possède une piece allié et bloque le passage
            }
        }
        return listeCaseAttaque;
    }


    @Override
    public String toString(){
        if(this.estBlanc()){
            return "F";
        }
        else{
            return "f";
        }
    }

}