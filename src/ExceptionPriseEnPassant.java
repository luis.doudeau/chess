
public class ExceptionPriseEnPassant extends Exception {


    private Coordonnee coo;
    private String [] coupSplit;

    public ExceptionPriseEnPassant(Coordonnee coo, String [] coupSplit){
        this.coo = coo;
        this.coupSplit = coupSplit;
    }

    public Coordonnee getCoo() {
        return coo;
    }

    public String[] getCoupSplit() {
        return coupSplit;
    }
}
