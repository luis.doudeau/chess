import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.layout.HBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ColorPicker;
import javafx.geometry.Insets;
import javafx.geometry.Pos;



public class ChessGraphic extends Application {
    
    // Scene principale 
    private BorderPane jeu;
    // Bandeau du haut de la fenêtre
    private HBox top;
    // Plateau de jeu
    private GridPane echequier;
    // Liste des boutons (cases)
    private List<List<Bouton>> boutons;
    // L'echequier
    private Plateau plateau;
    // case selectionner
    private String caseSelectionne; 

    private Jeu partie;


    @Override
    public void init(){
        this.jeu = new BorderPane();
        this.top = new HBox();
        this.echequier = new GridPane();
        this.boutons = new ArrayList<>();
        this.plateau = new Plateau();
        this.partie = new Jeu();
        this.caseSelectionne = "";
        this.top();
        this.poserBouton();
        this.jeu.setTop(top);
        this.jeu.setCenter(this.echequier);
        this.echequier.setAlignment(Pos.CENTER);  
    }


    public List<List<Bouton>> getBoutons() {
        return boutons;
    }
    public String getCaseSelectionne() {
        return caseSelectionne;
    }

    public GridPane getEchequier() {
        return echequier;
    }
    public BorderPane getJeu() {
        return jeu;
    }

    public Jeu getPartie() {
        return partie;
    }

    public Plateau getPlateau() {
        return plateau;
    }
    public void setPlateau(Plateau plateau){
        this.plateau = plateau;
    }
    public HBox getTop() {
        return top;
    }

    public void newPlateau(){
        this.plateau = new Plateau(); 
    }

    public void setCaseSelectionne(String caseSelectionne) {
        this.caseSelectionne = caseSelectionne;
    }
    

    public void top(){
        Label lb = new Label("Jeu d'echec");
        lb.setFont(Font.font("Comic Sans MS", FontPosture.ITALIC,70));
        this.top.getChildren().add(lb);
        this.top.setPadding(new Insets(20));
    }

    public void maj(){
        for(int ligne=0; ligne < 8; ligne ++){
            for(int colonne = 0; colonne < 8; colonne++){
                this.boutons.get(ligne).get(colonne).setLaCase(plateau.getCase(ligne, colonne));
                this.boutons.get(ligne).get(colonne).mettreImage();
            }
        }
    }


    public void poserBouton(){
        for(int ligne=0; ligne<8; ligne ++){
            List<Bouton> listeCase = new ArrayList<>();
            for(int colonne = 0; colonne<8; colonne++){
                Bouton bouton = new Bouton(this.plateau.getCase(ligne, colonne));
                bouton.setOnAction(new ControleurBouton(bouton, this));
                if(this.plateau.getCase(ligne, colonne).estBlanc()){
                    bouton.setBackground(new Background(new BackgroundFill(Paint.valueOf("#C8AC7F"), null, null)));}
                else{
                    bouton.setBackground(new Background(new BackgroundFill(Paint.valueOf("#B38B6D"), null, null)));
                }
                listeCase.add(bouton);
                this.echequier.add(bouton, colonne, ligne);
            }
            this.boutons.add(listeCase);
        }
    }

    @Override
    public void start(Stage stage){
        Scene scene = new Scene(this.jeu, 800, 800);
        stage.setTitle("Jeu d'echec");
        stage.setScene(scene);
        stage.setResizable(true);
        stage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }

    public void fenetrePromotion(Coordonnee coo){
        Button cavalier;
        Button fou;
        Button tour;
        Button dame;

        if(plateau.getCase(coo.getLigne(), coo.getColonne()).getPiece().estBlanc()){
            cavalier = new Button("",Bouton.chargeUneImage(Bouton.chargeImage().get("C")));
            fou = new Button("",Bouton.chargeUneImage(Bouton.chargeImage().get("F")));
            tour = new Button("",Bouton.chargeUneImage(Bouton.chargeImage().get("T")));
            dame = new Button("",Bouton.chargeUneImage(Bouton.chargeImage().get("D")));
        }
        else{
            cavalier = new Button("",Bouton.chargeUneImage(Bouton.chargeImage().get("c")));
            fou = new Button("",Bouton.chargeUneImage(Bouton.chargeImage().get("f")));
            tour = new Button("",Bouton.chargeUneImage(Bouton.chargeImage().get("t")));
            dame = new Button("",Bouton.chargeUneImage(Bouton.chargeImage().get("d")));
        }
        cavalier.setPrefSize(80, 80);
        cavalier.setOnAction(new ControleurPromotion(this, "C", coo));
        dame.setPrefSize(80, 80);
        dame.setOnAction(new ControleurPromotion(this, "D", coo));
        fou.setPrefSize(80, 80);
        fou.setOnAction(new ControleurPromotion(this, "F", coo));
        tour.setPrefSize(80, 80);
        tour.setOnAction(new ControleurPromotion(this, "T", coo));

        GridPane grillePromotion = new GridPane();
        grillePromotion.add(dame, 0, 0);    
        grillePromotion.add(tour, 0, 1);      
        grillePromotion.add(fou, 1, 0);  
        grillePromotion.add(cavalier, 1, 1);
        grillePromotion.setAlignment(Pos.CENTER);
        this.jeu.setCenter(grillePromotion);
    }

    public Alert alertEchecEtMat(boolean victoireBlanc){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Voulez-vous rejouer ?", ButtonType.YES, ButtonType.NO); 
        alert.setTitle("Jeu d'Echec");
        alert.setHeaderText("Echec et Mat");
        if(victoireBlanc){
            alert.setContentText("Les blancs ont gagnés !"+"\n"+"Voulez-vous rejouer une partie ?");
        }
        else{
            alert.setContentText("Les noirs ont gagnés !"+"\n"+"Voulez-vous rejouer une partie ?");
        }
        return alert;
    }

    public void rejouer() {
        this.echequier = new GridPane();
        this.boutons = new ArrayList<>();
        this.plateau = new Plateau();
        this.partie = new Jeu();
        this.caseSelectionne = "";
        this.echequier = new GridPane();
        this.echequier.setAlignment(Pos.CENTER);
        this.poserBouton();
        this.jeu.setCenter(this.echequier);
    }
}
