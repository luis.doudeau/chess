public class Coordonnee {

    private int ligne;
    private int colonne;

    public Coordonnee(int ligne ,int colonne){
        this.ligne = ligne;
        this.colonne = colonne;
    }

    public int getLigne() {
        return this.ligne;
    }

    public int getColonne() {
        return this.colonne;
    }    

    public void setColonne(int colonne) {
        this.colonne = colonne;
    }

    public void setLigne(int ligne) {
        this.ligne = ligne;
    }
}
