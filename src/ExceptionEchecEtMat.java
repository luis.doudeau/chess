
public class ExceptionEchecEtMat extends Exception {

    private boolean victoireBlanche;
    private String [] coupSplit;

    public ExceptionEchecEtMat(boolean victoireBlanche, String [] coupSplit){
        this.victoireBlanche = victoireBlanche;
        this.coupSplit = coupSplit;
    }

    public boolean getVictoireBlanche(){
        return this.victoireBlanche;
    }

    public String[] getCoupSplit() {
        return coupSplit;
    }
}
