import java.util.Set;

public interface InterfacePiece {
    
    public Set<Case> coupPossible(Plateau plateau);
    public abstract String toString();
    public boolean estBlanc();
    public void setColonne(int colonne);
    public void setLigne(int ligne);
    public int getLigne();
    public int getColonne();
    public String getName();
}
