import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javafx.scene.control.Button; 
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.image.Image;


public class Bouton extends Button{
    
    private Case laCase;

    public Bouton(Case laCase){
        this.laCase = laCase;
        this.setPrefSize(80, 80);
        mettreImage();
    }

    public Case getCase(){
        return this.laCase;
    }

    public void setLaCase(Case laCase) {
        this.laCase = laCase;
    }


    public void mettreImage(){
        HashMap<String, String> dicoImage = chargeImage();
        String img = dicoImage.get(laCase.affichagePiece());
        if(img != null){
            ImageView imagePiece = Bouton.chargeUneImage(img);
            this.setGraphic(imagePiece);
        }
        else{
            this.setGraphic(null);
        }
    }

    public static HashMap<String, String> chargeImage(){
        HashMap<String, String> dicoPieceImage = new HashMap<>();
        dicoPieceImage.put("P", "PionBlanc.png");
        dicoPieceImage.put("p", "PionNoir.png");
        dicoPieceImage.put("C", "CavalierBlanc.png");
        dicoPieceImage.put("c", "CavalierNoir.png");
        dicoPieceImage.put("F", "FouBlanc.png");
        dicoPieceImage.put("f", "FouNoir.png");
        dicoPieceImage.put("T", "TourBlanc.png");
        dicoPieceImage.put("t", "TourNoir.png");
        dicoPieceImage.put("D", "DameBlanc.png");
        dicoPieceImage.put("d", "DameNoir.png");
        dicoPieceImage.put("R", "RoiBlanc.png");
        dicoPieceImage.put("r", "RoiNoir.png");
        
        return dicoPieceImage;
    }

    public static ImageView chargeUneImage(String img){
        ImageView imagePiece = new ImageView(img);
        imagePiece.setFitHeight(50);
        imagePiece.setFitWidth(50);
        imagePiece.setPreserveRatio(true);
        return imagePiece;
    }

}
